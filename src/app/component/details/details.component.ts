import { Component, ViewChild, OnInit } from '@angular/core';
import { freeApiService } from "../../services/freeapi.service";
import { items } from "../../classes/data";
import { MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {
  [x: string]: any;
  constructor(private _freeApiService: freeApiService) { }

  lstitems:items[];

  btndisable:boolean=true
  checkValue:boolean=true
  submitValue:any=[]
  afterSubmit:any[]
  ngOnInit(){
 this._freeApiService.getData()
    .subscribe(
          data=>{
             this.lstitems = data;            
          }
    )
   
  }
  

  Search(){
    if(this.firstname != ""){
      this.lstitems = this.lstitems.filter(res=>{
        return res.firstname.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase());
    });
  }else if(this.firstname == ""){
    this.ngOnInit();
  }
  }
deleteRow(index:number){
  this.lstitems.splice(index, 1)
}
checked(data){
  this.submitValue.includes(data)?
  this.submitValue.splice(this.submitValue.indexOf(data),1):
  this.submitValue.push(data)

  this.btndisable=false
}
submitData(){
  this.afterSubmit=this.submitValue.map(x=>{
    return x
  })
  this.status=true
}
}
